# OpenLynx

OpenLynx is an open-source, quadruped robot based on the work of Spot of [Boston Dynamics](https://www.bostondynamics.com/spot "Offical site of Spot from Boston Dynamics") and the DIY quaduped robot from [Martin Triendl](https://www.youtube.com/channel/UClIHBZNAdeVpHsAlYC1lJfg "Youtube channel of Martin Triendl").


## Hardware

### Body
The entire body is designed using [FreeCAD](https://www.freecadweb.org/ "FreeCAD website"). All files can be found under hardware/freecad. The STL file for printing can be found under hardware/stl.

### Electronics

TODO

#### Microcontroller

TODO

#### Servos

TODO

#### Battery


## Software

TODO